﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gdevmat1718_01.Utilities
{
    class RandomNumberGenerator
    {
        private static readonly Random random = new Random();

        public static int GenerateInt(int min = 0, int max = 0)
        {
            return random.Next(min, max + 1);
        }

        public static double GenerateDouble(double min = 0, double max = 0)
        {
            return random.NextDouble() * (max - min) + min;
        }

        public static double GenerateGaussian(double mean = 0, double stdDev = 1)
        {
            var u1 = random.NextDouble();
            var u2 = random.NextDouble();

            var randomStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2);

            return mean = stdDev * randomStdNormal;
        }
    }
}
