﻿using Gdevmat1718_01.Models;
using Gdevmat1718_01.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Gdevmat1718_01
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Vector3 mousePosition = new Vector3();

        private Cube myCube = new Cube();
        private Circle myCircle = new Circle();
        private Vector3 wind = new Vector3(0.04f, 0, 0);
        private Vector3 wind2 = new Vector3(-0.05f, 0, 0);
        private Vector3 gravity = new Vector3(0, -0.5f, 0);
        private Vector3 helium = new Vector3(0, 0.05f, 0);
        private List<Circle> circles = new List<Circle>();
        private List<Circle> circles2 = new List<Circle>();
        private List<Cube> cubes = new List<Cube>();
        private List<Cube> cubes2 = new List<Cube>();
        private int time =0;
        



        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "Final";
            OpenGL gl = args.OpenGL;

            // Clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -60.0f);
       if(time == 360)
            {
                circles.Clear();
                for (int i = 1; i <= 4; i++)
                {
                    circles.Add(new Circle()
                    {
                        Mass = i,
                        red = RandomNumberGenerator.GenerateDouble(0, 1),
                        green = RandomNumberGenerator.GenerateDouble(0, 1),
                        blue = RandomNumberGenerator.GenerateDouble(0, 1),
                        alpha = 0.6d,
                        Position = new Vector3(40, 23, 0)

                    });
                }
                time = 0;
            }
            foreach (var c in circles)
            {
                c.Render(gl);
                if(c.Position.x >= -40)
                    c.ApplyForce(wind);
                if (c.Position.x <= 40)
                    c.ApplyForce(wind2);
                c.ApplyGravity();
                
/*
                var coefficient = 0.01f;
                var normal = 1;
                var frictionMagnitude = coefficient * normal;
                var friction = c.Velocity;
                friction *= -1;
                friction.Normalize();
                friction *= frictionMagnitude;

                c.ApplyForce(friction);
*/
                if (c.Position.y <= -23)
                {
                    c.Position.y = -23;
                    c.Velocity.y *= -1.0f;
                }
                if (c.Position.x >= 45 || c.Position.x <= -45)
                {
                    c.Velocity.x *= -1.0f;
                }

               

            }
                  

            foreach (var v in cubes)
            {
                foreach(var z in circles)
                {
                   if( z.HasCollidedWith(v))
                    {
                        z.Velocity.y *= -1.0f;
                    }
                }
                v.Scale = new Vector3(3,1 ,0 );
                v.Velocity.x = 2;
                v.Velocity.y = 0.01f;
                if(v.Position.x >= 40 || v.Position.x <= -40)
                {
                    v.Position.x *= -1.0f;
                }
                
                v.Render(gl);
            }

            time++;
          


            /*
                        foreach (var c in cubes )
                        {
                            c.ApplyForce(wind);
                            c.ApplyGravity();
                            c.Scale = new Vector3(c.Mass / 2, c.Mass / 2, c.Mass / 2);
                            //Friction
                            var coefficient = 0.01f;
                            var normal = 1;
                            var frictionMagnitude = coefficient * normal;
                            var friction = c.Velocity;
                            friction *= -1;
                            friction.Normalize();
                            friction *= frictionMagnitude;

                            c.ApplyForce(friction);

                            if (c.Position.y <= -23)
                            {
                                c.Position.y = -23;
                                c.Velocity.y *= -1.0f;
                            }

                            if (c.Position.x >= 45 || c.Position.x <= -45)
                            {
                                c.Velocity.x *= -1.0f;
                            }

                            c.Render(gl);
                        }
            */

            /*
            baydersMind = RandomNumberGenerator.GenerateInt(0, 20);

            if (baydersMind == 3)
            {
                myCircle.ApplyForce(darthBayder);
                the object will move towards the mouse position desu <3;
            }
            */

        }
     
        #region INITIALIZATION

        public MainWindow()
        {
            InitializeComponent();
            
            for (int i = 1; i <= 4; i++)
            {
                circles.Add(new Circle()
                {
                    Mass = i, 
                    red = RandomNumberGenerator.GenerateDouble(0, 1),
                    green = RandomNumberGenerator.GenerateDouble(0, 1),
                    blue = RandomNumberGenerator.GenerateDouble(0, 1),
                    alpha = 0.6d,
                    Position = new Vector3(40, 23, 0)

                });
            }

           /* for (int i = 1; i <= 10; i++)
            {
                circles2.Add(new Circle()
                {
                    Mass = i,
                    red = RandomNumberGenerator.GenerateDouble(0, 1),
                    green = RandomNumberGenerator.GenerateDouble(0, 1),
                    blue = RandomNumberGenerator.GenerateDouble(0, 1),
                    alpha = 0.6d,
                    Position = new Vector3(0, 23, 0)

                });
            }*/

            for (int i = 1; i <= 16; i++)
            {
                cubes.Add(new Cube()
                {
                    Mass = 5,
                    Position = new Vector3(-40 + (i*5), 16 - (i*10), 0)
                });
                 
            }
           


        }

        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }

        private void OpenGLControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
        #endregion

        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
        }
    }
}
