﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpGL;

namespace Gdevmat1718_01.Models
{
    public class Circle : Moveable
    {
        private const int TOTAL_CIRCLE_ANGLE = 360;

        public float radius = 1.0f;

        public Circle()
        {

        }

        public Circle(Vector3 initPos)
        {
            this.Position = initPos;
        }
        public override void Render(OpenGL gl)
        {
            gl.PointSize(2.0f);
            gl.Color(red, green, blue, alpha);
            gl.Begin(OpenGL.GL_LINES);

            for (int i = 0; i < TOTAL_CIRCLE_ANGLE; i++)
            {
                gl.Vertex(Position.x, Position.y);
                gl.Vertex(Math.Cos(i) * radius + Position.x, Math.Sin(i) * radius + Position.y);
            }
            gl.End();

            UpdateMotion();
        }
        private void UpdateMotion()
        {
            this.Velocity += this.Acceleration;
            this.Position += this.Velocity;
            this.Acceleration *= 0;
        }
        public bool HasCollidedWith(Cube target)
        {
            bool xHasNotCollided =
                this.Position.x - this.Scale.x > target.Position.x + target.Scale.x ||
                this.Position.x + this.Scale.x < target.Position.x - target.Scale.x;

            bool yHasNotCollided =
                this.Position.y - this.Scale.y > target.Position.y + target.Scale.y ||
                this.Position.y + this.Scale.y < target.Position.y - target.Scale.y;

            bool zHasNotCollided =
                this.Position.z - this.Scale.z > target.Position.z + target.Scale.z ||
                this.Position.z + this.Scale.z < target.Position.z - target.Scale.z;

            return !(xHasNotCollided || yHasNotCollided || zHasNotCollided);

        }
    }
}
