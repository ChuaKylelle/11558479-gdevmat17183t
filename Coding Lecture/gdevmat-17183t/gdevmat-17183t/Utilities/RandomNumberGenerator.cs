﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat_17183t.Utilities
{
    public class RandomNumberGenerator
    {
        private static readonly Random random = new Random();

        public static int GenerateInt(int min = 0, int max = 0)
        {
            return random.Next(min, max + 1);
        }

        public static double GenerateDouble(double min = 0, double max = 0 )
        {
            return random.NextDouble() * (max - min) + min;
        }
    }
}
