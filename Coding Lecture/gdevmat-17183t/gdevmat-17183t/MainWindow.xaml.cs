﻿using SharpGL;
using SharpGL.SceneGraph.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace gdevmat_17183t
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const float LINE_SMOOTHNESS = 0.02f;
        private const float GRAPH_LIMIT = 15;
        private const int TOTAL_CIRCLE_ANGLE = 360;
        public MainWindow()
        {
            InitializeComponent();
        }

        //float rotation = 0;
        private float frequency = 2.0f;
        private float amplitude = 1.0f;
        private int time = 0;
        
        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 1";
            OpenGL gl = args.OpenGL;

            // Clear the screen and the depth buffer 
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -40.0f);

            
            /*gl.Rotate(rotation, 0.0f, 1.0f, 0.0f);
            Teapot tp = new Teapot();
            tp.Draw(gl, 14, 1, OpenGL.GL_FILL);

            rotation += 3.0f;*/

            //Draw point
            gl.PointSize(10.0f);
            gl.Begin(OpenGL.GL_POINTS);
            gl.Vertex(-3, 5);
            gl.End();

            //Draw line
            gl.LineWidth(2.0f);
            gl.Begin(OpenGL.GL_LINE_STRIP);
            gl.Vertex(-15, 0);
            gl.Vertex(15, 0);
            gl.End();
            for (int i = -15; i <= 15; i++)
            {
                gl.LineWidth(1.0f);
                gl.Begin(OpenGL.GL_LINE_STRIP);
                gl.Vertex(-0.5, i);
                gl.Vertex(0.5, i);
                gl.End();

                gl.LineWidth(1.0f);
                gl.Begin(OpenGL.GL_LINE_STRIP);
                gl.Vertex(i, 0.5);
                gl.Vertex(i, -0.5);
                gl.End();
            }

            gl.LineWidth(2.0f);
            gl.Begin(OpenGL.GL_LINE_STRIP);
            gl.Vertex(0, 15);
            gl.Vertex(0, -15);
            gl.End();

            for(int i = -15; i <= 15; i++)
            {
                gl.LineWidth(1.0f);
                gl.Begin(OpenGL.GL_LINE_STRIP);
                gl.Vertex(i, 0.5);
                gl.Vertex(i, 0.5);
                gl.End();

                gl.LineWidth(1.0f);
                gl.Begin(OpenGL.GL_LINE_STRIP);
                gl.Vertex(0.5, i);
                gl.Vertex(-0.5, i);
                gl.End();
            }

            //f(x) = 2x +3 
            /*
             * Linear Function
             * Let x be 4, then y = 11
             * Let x be -3. then y = 3 
             */
            gl.PointSize(1.0f);
            gl.Begin(OpenGL.GL_POINTS);
            for (float x = -(GRAPH_LIMIT - 5); x <= (GRAPH_LIMIT - 5); x += LINE_SMOOTHNESS)
            {
                gl.Vertex(x, (2 * x) + 3);
            }
            gl.End();

            /* Quadratic Function
             * f(x) = x ^ 2 + 3x-4
             * let x = 3,14 
             */
            gl.PointSize(1.0f);
            gl.Begin(OpenGL.GL_POINTS);
            for (float x = -(GRAPH_LIMIT - 5); x <= (GRAPH_LIMIT - 5); x += LINE_SMOOTHNESS)
            {
                gl.Vertex(x, (x * x) + (3 * x) - 4);
            }
            gl.End();

            /*
             * Circle 
             * radius
             * cos and sin
             */

            float radius = 5.0f;
            gl.PointSize(1.0f);
            gl.Begin(OpenGL.GL_POINTS);
            for(int i = 0; i <= TOTAL_CIRCLE_ANGLE; i++)
            {
                gl.Vertex(Math.Cos(i) * radius, Math.Sin(i) * radius);
            }
            gl.End();

            /*
             * Sine wave
             * 
             */

            gl.Begin(OpenGL.GL_POINTS);
            
            for (double x = -10; x <= 10; x += LINE_SMOOTHNESS)
            {
              gl.Vertex(x, (amplitude * Math.Sin(frequency * x + time)));
            }
            if (Keyboard.IsKeyDown(Key.W)) amplitude++;
            if (Keyboard.IsKeyDown(Key.S)) amplitude--;
            if (Keyboard.IsKeyDown(Key.A)) frequency++;
            if (Keyboard.IsKeyDown(Key.D)) frequency--;
            gl.End();

            time += 1;
            if (time >= 360) time = 0;
        }



        #region OPENGL INITIALIZTION
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
        #endregion
    }
}
