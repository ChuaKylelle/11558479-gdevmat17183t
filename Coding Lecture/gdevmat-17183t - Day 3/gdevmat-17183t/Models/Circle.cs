﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpGL;

namespace gdevmat_17183t.Models
{
    public class Circle : Movable
    {
        private const int TOTAL_CIRCLE_ANGLE = 360;

        public float radius = 1.0f;
        public override void Render(OpenGL gl)
        {
            gl.PointSize(2.0f);
            gl.Color(red, green, blue, alpha);
            gl.Begin(OpenGL.GL_LINES);
            for(int i = 0; i <= TOTAL_CIRCLE_ANGLE; i++)
            {
                gl.Vertex(posX, posY);
                gl.Vertex(Math.Cos(i) * radius + posX, Math.Sin(i) * radius + posY);
            }
            gl.End();
        }
    }
}
