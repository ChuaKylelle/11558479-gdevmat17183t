﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using gdevmat_17183t.Models;
using gdevmat_17183t.Utilities;

namespace gdevmat_17183t
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private Cube myCube = new Cube()
        {
            posX = -5,
            posY = 0
        };

        private Circle myCircle = new Circle()
        {
            posX = 5,
            posY = 0
        };

        private List<Circle> circles = new List<Circle>();
        private List<Cube> cube = new List<Cube>();
        private int frame = 0;
        /*
        private const int NORTH = 0;
        private const int SOUTH = 1;
        private const int EAST = 2;
        private const int WEST = 3;
        private const int NORTHWEST = 4;
        private const int NORTHEAST = 5;
        private const int SOUTHWEST = 6;
        private const int SOUTHEAST = 7;
        private const int BASE = 8;

        private const int RED = 9;
        private const int GREEN = 10;
        private const int BLUE = 11;
       


        private Cube myFirstCube = new Cube(-5, 3);
        */


        public MainWindow()
        {
            InitializeComponent();
        }

     
        
        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 3";
            OpenGL gl = args.OpenGL;

            // Clear the screen and the depth buffer 
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100.0f);

            circles.Add(new Circle()
            {
                red = 0.6d,
                green = 0.3d,
                blue = 0.6d,
                alpha = 0.1d,
                radius = RandomNumberGenerator.GenerateInt(1, 6),
                posX = (float)RandomNumberGenerator.GenerateGaussian(0, 15),
                posY = (float)RandomNumberGenerator.GenerateGaussian(0, 15)
                
            });
            
            cube.Add(new Cube()
            {
                red = RandomNumberGenerator.GenerateDouble(0, 1),
                green = RandomNumberGenerator.GenerateDouble(0, 1),
                blue = RandomNumberGenerator.GenerateDouble(0, 1),
                alpha = RandomNumberGenerator.GenerateDouble(0,1),
                posX = (float)RandomNumberGenerator.GenerateGaussian(0, 50),
                posY = (float)RandomNumberGenerator.GenerateGaussian(-50, 50)
            });
            foreach(var c in circles)
            {
                c.Render(gl);
             
            }

            foreach(var d in cube)
            {
                d.Render(gl);
            }

            frame++;
            if(frame >= 300)
            {
                circles.Clear();
                cube.Clear();
                frame = 0; 
            }
            



        }



        #region OPENGL INITIALIZTION
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
        #endregion
    }
}
