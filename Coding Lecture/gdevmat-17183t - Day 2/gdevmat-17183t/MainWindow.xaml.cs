﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using gdevmat_17183t.Models;
using gdevmat_17183t.Utilities;

namespace gdevmat_17183t
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int NORTH = 0;
        private const int SOUTH = 1;
        private const int EAST = 2;
        private const int WEST = 3;
        private const int NORTHWEST = 4;
        private const int NORTHEAST = 5;
        private const int SOUTHWEST = 6;
        private const int SOUTHEAST = 7;
        private const int BASE = 8;

        private const int RED = 9;
        private const int GREEN = 10;
        private const int BLUE = 11;
       


        private Cube myFirstCube = new Cube(-5, 3);
        
        public MainWindow()
        {
            InitializeComponent();
        }

     
        
        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 2";
            OpenGL gl = args.OpenGL;

            // Clear the screen and the depth buffer 
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -40.0f);

            myFirstCube.Render(gl);

            gl.DrawText(0, 0, 1, 1, 1, "Arial", 25, RandomNumberGenerator.GenerateInt(0,5) + "");

           /* int outcome = RandomNumberGenerator.GenerateInt(0,8);
            switch(outcome)
            {
                case NORTH:
                    myFirstCube.posY++;
                    
                    break;

                case SOUTH:
                    myFirstCube.posY--;
                    break;

                case EAST:
                    myFirstCube.posX++;
                    break;

                case WEST:
                    myFirstCube.posX--;
                    break;

                case NORTHWEST:
                    myFirstCube.posX--;
                    myFirstCube.posY++;
                    break;

                case NORTHEAST:
                    myFirstCube.posX++;
                    myFirstCube.posY++;
                    break;

                case SOUTHWEST:
                    myFirstCube.posX--;
                    myFirstCube.posY--;
                    break;

                case SOUTHEAST:
                    myFirstCube.posX++;
                    myFirstCube.posY--;
                    break;

                case BASE:
                    gl.Color(0.0, 0.0, 0.0);        
                    break;         
            }

            myFirstCube.color = new Models.Color(RandomNumberGenerator.GenerateDouble(0, 1), 
                RandomNumberGenerator.GenerateDouble(0, 1), 
                RandomNumberGenerator.GenerateDouble(0, 1));
            /*int colour = RandomNumberGenerator.GenerateInt(9, 11);
            switch(colour)
            {
                case RED:
                    gl.Color(1.0, 0.0, 0.0);
                   break;

                case GREEN:
                    gl.Color(0.0, 1.0, 0.0);
                    break;

                case BLUE:
                    gl.Color(0.0, 0.0, 1.0);
                    break;
            }
            */

            double outcomeWithDiffProbability = RandomNumberGenerator.GenerateDouble(0, 1);

            if (outcomeWithDiffProbability < 0.2)
            {
                // North
                myFirstCube.posY++;

            }
            else if (outcomeWithDiffProbability > 0.2 && outcomeWithDiffProbability < 0.4)
            {
                //South
                myFirstCube.posY--;
            }
            else if (outcomeWithDiffProbability > 0.4 && outcomeWithDiffProbability < 0.6)
            {
                //Left
                myFirstCube.posX--;
            }
            else
                //Right
                myFirstCube.posX++;

            // 20% chance moving up
            // 20% chance moving down
            // 20% chance moving left
            // 40% chance moving right
        }



        #region OPENGL INITIALIZTION
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
        #endregion
    }
}
