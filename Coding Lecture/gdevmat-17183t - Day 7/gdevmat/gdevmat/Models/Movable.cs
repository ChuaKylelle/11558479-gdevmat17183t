﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat.Models
{
    public class Movable
    {
        public Vector3 Position = new Vector3();
        public Vector3 Velocity = new Vector3();
        public Vector3 Acceleration = new Vector3();
        public Color color = new Color();
        public float Mass = 1.0f;

        public void ApplyGravity(float scalar = 0.1f)
        {
            this.Acceleration += (new Vector3(0, -scalar * Mass, 0) / Mass);
        }
        public void ApplyForce(Vector3 force)
        {
            // F = MA
            // A = F/M
            this.Acceleration += (force / Mass); //force accumulation
        }

        public virtual void Render(OpenGL gl)
        {
            gl.Color(color.r, color.g, color.b, color.a);
            UpdateMotion();
        }

        private void UpdateMotion()
        {
            this.Velocity += this.Acceleration;
            this.Position += this.Velocity;
            this.Acceleration *= 0;
        }
    }
}
