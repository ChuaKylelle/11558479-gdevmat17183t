﻿using gdevmat.Models;
using gdevmat.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace gdevmat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Initialization
        public MainWindow()
        {
            for(int i = 1; i<=10; i++)
            {
                //if (frameCube >= 2)
                //{
                myCube.Add(new Cube()
                {
                    Scale = new Vector3(i / 2, i / 2, i / 2),
                    Position = new Vector3((float)RandomNumberGenerator.GenerateGaussian(1, 15), 45, 0),
                    Mass = i,
                    color = new Models.Color(RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1)),
                });
                //frameCube = 0;
                //}
            }
            frameCounter++;
            
            InitializeComponent();
        }

        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);
            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }

        #endregion

        private Vector3 mousePosition = new Vector3();

        private Vector3 wind = new Vector3(0.05f, 0, 0);
        private Vector3 gravity = new Vector3(0, -0.5f, 0);

        List<Cube> myCube = new List<Cube>();
        List<Circle> myCircle = new List<Circle>();

        private float gravityValue = 0.1f;
       // private int frameCircle = 0;
        private int frameCube = 0;
        private int frameCounter = 0;

       // private float mass = 1.0f;
        

        public Vector3 MousePosition { get => mousePosition; set => mousePosition = value; }

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            // Clear The Screen And The Depth Buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);

            // Move Left And Into The Screen
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100.0f);

         

           //frameCircle++;
           
           frameCube++;
            /*
           if(frameCircle >= 2)
            {
                myCircle.Add(new Circle()
                {
                    radius = RandomNumberGenerator.GenerateInt(1, 5),
                    Position = new Vector3((float)RandomNumberGenerator.GenerateGaussian(1, 15), 45, 0),
                    Mass = mass,
                    color = new Models.Color(RandomNumberGenerator.GenerateDouble(0,1),RandomNumberGenerator.GenerateDouble(0,1), RandomNumberGenerator.GenerateDouble(0,1), RandomNumberGenerator.GenerateDouble(0,1)),
                });
                frameCircle = 0;
            }*/

           /* if (frameCounter == 300)
            {
                myCube.Clear();
            }
            if (myCube.Count <= 10)
            {
                //if (frameCube >= 2)
                //{
                    myCube.Add(new Cube()
                    {
                        Scale = new Vector3(mass/2,mass/2,mass/2),
                        Position = new Vector3((float)RandomNumberGenerator.GenerateGaussian(1, 15), 45, 0),
                        Mass = mass,
                        color = new Models.Color(RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1)),
                    });
                    frameCube = 0;
                //}
            }
            frameCounter++;
            mass++;*/

      
           /*
            foreach (var c in myCircle)
            {
                c.Render(gl);
                c.ApplyForce(wind);
                c.ApplyForce(gravity);
               /c.ApplyForce(mousePosition);
               
                if (c.Position.y <= -25)
                {
                    c.Velocity.y *= -1;
                }
            }*/
            
            foreach (var d in myCube)
            {
                d.Render(gl);
                d.ApplyForce(wind);
                //d.ApplyForce(gravity);
                d.ApplyGravity(gravityValue);
                // d.ApplyForce(mousePosition);

                // Friction = -uNv
                var coefficient = 0.01f;
                var normal = 1;
                var frictionMagnitude = coefficient * normal;
                var friction = d.Velocity;
                friction *= -1;
                friction.Normalize();
                friction *= frictionMagnitude;
                d.ApplyForce(friction);
                if (d.Position.y <= -25)
                {
                    d.Velocity.y *= -1;
                }
                if (d.Position.x <= -40)
                {
                    d.Velocity.x *= -1;
                }
                if (d.Position.x <= 40)
                {
                    d.Velocity.x *= -1;
                }

            }

          
            
         
        }

        #region Mouse Func
        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            MousePosition.x = (float)position.X - (float)Width / 2.0f;
            MousePosition.y = -((float)position.Y - (float)Height / 2.0f);
            MousePosition.Normalize();
        }
        #endregion
    }
}
