﻿using gdevmat_17183t.Models;
using gdevmat_17183t.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;


namespace gdevmat_17183t
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Vector3 mousePosition = new Vector3();

        private Cube myCube = new Cube()
        {
            Position = new Vector3(-40, -15, 0),

            Mass = 1
        };
        private Circle myCircle = new Circle()
        {
            Position = new Vector3(-40, 0, 0),

            Mass = 20

        };
        private Vector3 wind = new Vector3(0.05f, 0, 0);

        private Vector3 gravity = new Vector3(0, -0.1f, 0);
        private Vector3 helium = new Vector3(0, 0.1f, 0);
        private Vector3 bounce = new Vector3(0, 7.0f, 0);


        int count = 0;

        public Cube MyCube { get => myCube; set => myCube = value; }

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 6";
            OpenGL gl = args.OpenGL;

            // Clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -60.0f);

            count++;
            if (count >= 300)
            {
                count = 0;
                MyCube.Position = new Vector3(-40, -15, 0);
                myCircle.Position = new Vector3(-40, 0, 0);
                myCircle.Position = new Vector3();
                MyCube.Velocity = new Vector3();

            }
            /*  myCube.Render(gl);

              myCube.ApplyForce(wind);
              myCube.ApplyForce(gravity);
              myCube.Velocity *= 0.5f;
              if(myCube.Position.y <= -20)
              {
                  myCube.ApplyForce(bounce);
              }*/

            MyCube.Render(gl);
            MyCube.ApplyForce(wind);
            MyCube.ApplyForce(gravity);

            if (MyCube.Position.y <= -25)
            {
                MyCube.Velocity.y *= -1;
            }

            if (MyCube.Position.x >= 40)
            {
                MyCube.Velocity.x *= -1;
            }

            myCircle.Render(gl);
            myCircle.ApplyForce(wind);
            myCircle.ApplyForce(helium);
            if (myCircle.Position.y >= 20)
            {
                myCircle.Velocity.y *= -1;
            }
            if (myCircle.Position.y <= 0)
            {
                myCircle.Velocity.y *= -1;
            }
            if (myCircle.Position.x >= 30)
            {
                myCircle.Velocity.x *= -1;
            }
            if (myCircle.Position.x <= -20)
            {
                myCircle.Velocity.x *= -1;
            }







        }

        #region INITIALIZATION
        public MainWindow()
        {
            InitializeComponent();

        }


        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);


        }

        #endregion

        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
        }
    }
}


