﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat_17183t.Models
{
    public class Cube : Movable
    {
        public Cube(float x = 0, float y = 0, float z = 0)
        {
            this.Position.x = x;
            this.Position.y = y;
            this.Position.z = z;
        }

        public Cube(Vector3 initPos)
        {
            this.Position = initPos;
        }

        public override void Render(OpenGL gl)
        {
            gl.Color(red, green, blue, alpha);
            gl.Begin(OpenGL.GL_QUADS);
            // Front face
            gl.Vertex(this.Position.x - 0.5f, this.Position.y + 0.5f);
            gl.Vertex(this.Position.x + 0.5f, this.Position.y + 0.5f);
            gl.Vertex(this.Position.x + 0.5f, this.Position.y - 0.5f);
            gl.Vertex(this.Position.x - 0.5f, this.Position.y - 0.5f);

            gl.End();

            UpdateMotion();
        }

        private void UpdateMotion()
        {
            this.Velocity += this.Acceleration;
            this.Position += this.Velocity;
            //this.Acceleration *= 0;
        }
    }
}
